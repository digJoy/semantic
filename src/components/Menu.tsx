import * as React from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

export default class BasicMenu extends React.Component {
  public state = { activeItem: '' }

  public handleItemClick = (e: any, name: any) => this.setState({ activeItem: name })

  public render() {
    const { activeItem } = this.state
    return (
      <Menu secondary style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-start' }}>
            <Menu.Item name='1' active={activeItem === '1'} onClick={this.handleItemClick}>
                <Icon name="home" />Menu  1
            </Menu.Item>
            <Menu.Item name='2' active={activeItem === '2'} onClick={this.handleItemClick}>
                <Icon name="cloud upload" /> Menu  2
            </Menu.Item>
            <Menu.Item name='3' active={activeItem === '3'} onClick={this.handleItemClick}>
               <Icon name="exchange" /> Menu  3
            </Menu.Item>
      </Menu>
    )
  }
}
