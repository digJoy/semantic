export default {
    colors: {
        primary: '#c62828',
        primaryLight: '#ff5f52',
        primaryDark: '#8e0000',
        secondary: '#1976d2',
        light: '#eee'
    }
}