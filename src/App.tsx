import * as React from 'react';
import 'semantic-ui-css/semantic.min.css'
// import theme from './theme/theme';
import { Header, Icon, Button, Dropdown, Sidebar, Segment, Menu, Grid } from 'semantic-ui-react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

import BasicMenu from './components/Menu';

const trigger = (
  <span>
    <Icon name='user' /> Joyce
  </span>
)

const options = [
  {
    key: 'user',
    text: (
      <span>
        Último acesso às <strong>Sex 09:32</strong>
      </span>
    ),
    disabled: true,
  },
  { key: 'settings', text: 'Configurações' },
  { key: 'sign-out', text: 'Sair' },
]

export const styles = {
  line: {
    borderRight: '1px solid #333',
    width: 200
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#444',
    height: 51,
    padding: 10,
    marginTop: 10,
  },
  side: {
    width: 200,
    borderRight: '1px solid #eee',
    display: 'flex',
    justifyContent: 'flex-start'
  } as React.CSSProperties,
  info: {
    display: 'flex',
    alignSelf: 'flex-end',
    alignItems: 'center',
    color: '#fff'
  } as React.CSSProperties,
  footer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  } as React.CSSProperties,
  search: {
    backgroundColor: '#666'
  } as React.CSSProperties
}

@observer
export default class App extends React.Component {

  // @observable private sideMenu: any = 2;
  // @observable private screen: any = 14; 
  @observable private clicked = false;
  @observable private visible = false;

  // private handleSideMenu = action(() => {
  //   this.clicked = !this.clicked;
  //   if (this.clicked === true) {
  //       this.sideMenu = 1, this.screen = 15;
  //   } else {
  //     this.sideMenu = 2, this.screen = 14;
  //    }
  // })

  private showSidebar = action(() => this.visible = true);
  private hideSidebar = action(() => this.visible = false);

  private handleSidebar = action(() => {
    this.visible = !this.visible;
    this.visible === true ? this.showSidebar() : this.hideSidebar();
  })

  public render() {
    return (
      <>
        <Sidebar.Pushable style={{ height: '100vh' }} as={Segment}>
          <Sidebar
            as={Menu}
            animation='slide along'
            icon='labeled'
            inverted
            onHide={this.hideSidebar}
            vertical
            visible={this.visible}
            width='thin'
          >
            {/* <Grid.Column style={{ animation: 'slide 0.5s forwards', height: '100vh', color: '#fff', backgroundColor: '#00114f', justifyContent: 'center' }} width={this.sideMenu}> */}
            <div style={{ display: 'flex', justifyContent: 'center', padding: 10 }}>
              <h2>Sistema</h2>
            </div>

            {/* <div style={{  backgroundColor: '#fff', padding: 10 }}>
              <h5> <Icon name="search" size="small" /> Pesquisar </h5>
            </div> */}

            <BasicMenu />
            {/* </Grid.Column> */}
          </Sidebar>
          <Sidebar.Pusher>
            <Segment basic>
              <div style={{ display: 'flex', padding: 0, margin: 0, position: 'absolute', marginLeft: 188, marginTop: 16, zIndex: 1 }}>
                <Button
                  small
                  circular
                  style={{ backgroundColor: '#8e0000', color: '#fff' }}
                  onClick={this.handleSidebar}
                  icon={this.clicked ? "angle right" : "angle left"}
                />
              </div>
              <Grid>
                <Grid.Column style={{ padding: 0, backgroundColor: '#eee', margin: 0 }} width={this.visible ? 14 : 16}>
                  <Header style={styles.header}>
                    <div style={{ color: '#eee', paddingRight: 50 }}>
                      <h4> <Dropdown trigger={trigger} options={options} /> </h4>
                      {/* <h4>Joyce Santos <Icon name="angle down" /> </h4>{ /** <Icon name="user circle" size="big" />  */}
                    </div>
                  </Header>
                  <div style={{ height: 42 }}>
                    submenu
                      </div>
                  <main style={{ borderLeft: '1px solid #444' }}>
                    {this.props.children}
                  </main>
                </Grid.Column>
              </Grid>
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>



      </>
    );
  }
}
